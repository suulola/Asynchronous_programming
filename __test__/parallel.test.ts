import { runInParallel } from "../index";

describe('Run in Parallel', () => {

  const urls = [
    'https://jsonplaceholder.typicode.com/posts/1',
    'https://jsonplaceholder.typicode.com/posts/2',
    'https://jsonplaceholder.typicode.com/posts/3',
    'https://jsonplaceholder.typicode.com/posts/4',
    'https://jsonplaceholder.typicode.com/posts/5',
    'https://jsonplaceholder.typicode.com/posts/6',
    'https://jsonplaceholder.typicode.com/posts/7',
    'https://jsonplaceholder.typicode.com/posts/8',
    'https://jsonplaceholder.typicode.com/posts/9',
  ]

  test('check when url is undefined', async () => {
    const data = await runInParallel({ urls: undefined, concurrency: 2 });
    expect(data).toStrictEqual([]);
  });
  test('check when concurrency is 0', async () => {
    const data = await runInParallel({ urls, concurrency: 0 });
    expect(data).toStrictEqual([]);
  });

  test('if 6 url is supplied', async () => {
    const data = await runInParallel({ urls: urls.slice(0, 6), concurrency: 2 });
    expect(data.length).toBe(6);
  });

  test('if 6 url is supplied with the update method with a concurrency of 2, update method should be called 3 times', async () => {
    const updateResult = jest.fn()
    await runInParallel({ urls: urls.slice(0, 6), concurrency: 2, updateMethod: updateResult });
    expect(updateResult).toHaveBeenCalledTimes(3);
  });
  test('if 9 url is supplied with the update method with a concurrency of 4, update method should be called 3 times', async () => {
    const updateResult = jest.fn()
    await runInParallel({ urls, concurrency: 4, updateMethod: updateResult });
    expect(updateResult).toHaveBeenCalledTimes(3);
  });

})

