import axios from 'axios'

// 1. Using Promise.all([]) 
// Because Javascript is single threaded, having multiple request served at the same time in parallel is technically impossible - Promise.all can however by used to wait for the concurrent request response
// The major tradeoff of using Promise.all is that the failure of one request invalidates the entire concurrent request for that cycle.

// A sample updateMethod method that can be passed.
let result: string[] = []
const updateResult = (response: string[]) => {
  result = response
}
// In the case of React - const [result, setResult] = useState([])

type ParallelRequestType = {
  urls: string[]
  concurrency: number
  updateMethod?: (value: string[]) => void
}

export const runInParallel = async (options: ParallelRequestType): Promise<string[]> => {
  const { urls, concurrency, updateMethod } = options;
  // The updateMethod is useful to keep updating a parent source when the data updates

  let allData: string[] = [];

  if (!urls || urls.length === 0 || concurrency == 0) return allData;

  const numberOfCycle = Math.ceil(urls.length / concurrency);

  for (let i = 0; i < numberOfCycle; i++) {

    const startingPoint = i * concurrency;
    const endpointPoint = (i + 1) * concurrency;
    const urlToFetch = urls.slice(startingPoint, endpointPoint);

    const promises: Promise<string>[] = urlToFetch.map((url: string) =>
      axios(url, { responseType: 'text' }).then((response) => response.data)
      // fetch(url).then(response => response.text()) would be more appropriate in a browser environment.
    );
    const data = await Promise.all(promises);
    allData = [...allData, ...data]
    if (updateMethod) updateMethod(allData);
  }

  return allData;

}




